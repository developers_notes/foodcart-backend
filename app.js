var express = require('express');
var crypto = require('crypto');
var cors = require('cors')
var _ = require('lodash');
var Client = require('node-rest-client').Client;
var jsSHA = require('jsSHA');
//const dao = require('./dao');
const cons = require('./config/constants');
const { glob } = cons;
//const { models } = dao;
//const { m } = models;
const restifyErrors = require('restify-errors');
var server = require('http').createServer(app);
var app = express();

var client = new Client();
var salt = getHash(Math.random().toString(36).substring(7));
const io = require('socket.io')(server, {
  cors: {
    origin: '*',
  }
});
let existing = []; // existing username is being processed in sockets layer
var err = {
  error: true,
  type: "",
  desc: "",
}
client.registerMethod("getAllUser", `${glob.url}/user`, "GET");
client.registerMethod("postMethod", `${glob.url}/user`, "POST");

io.on('connection', function(socket) {
  console.log('user connected');
  // Initialize defaults from DB
  getUsers();

  socket.on('process', (message) => {
    var args = {
      data: { test: "hello" },
      headers: { "Authorization": `Bearer ${glob.token}`, "Content-Type": "application/json"}
    };
    if (message && message.command === 'register') {
      if (_.indexOf(existing, message.username) > -1) {
        err.type = "username";
        err.desc = "Username Exist";
        io.emit('process', err);
        return;
      }
      
      const hash = getHash(message.hashedPassword);
      let payload = message;
      payload.password = hash;
      args.data = {data: payload};
      saveToDB(args);
      
    } else if (message && message.command === 'loginSalt') {
      //if exist in the databasea
      client.get(`${glob.url}/user/${message.username}`, args, function (data, response) {
        io.emit('process', data);
        if (data && data[0] && data[0].code === 404) {
          err.type = "SQL";
          err.desc = data[0].details ? data[0].details : data[0].message;
          io.emit('process', err);
        } else {
          const payload =  {
          "command": "loginSalt",
          "username": message.username,
          "validity": 300,
          "salt": salt,
         }
         io.emit('process', payload);
        }
      });
    } else if (message && message.command === 'login') {

      client.get(`${glob.url}/user/${message.username}`, args, function (data, response) {
        //const withSalt = getHash(message.hashedPassword);

        const shaObj = new jsSHA('SHA-256', 'TEXT');
        shaObj.setHMACKey(message.password, 'TEXT');
        shaObj.update(data.username);
        let hash = shaObj.getHash("HEX");
        //hash = getHash(hash);

        const shaObj2 = new jsSHA('SHA-256', 'TEXT');
        shaObj2.setHMACKey(salt, 'TEXT');
        shaObj2.update(hash);
        const serverHash = shaObj2.getHash("HEX");

        const ret  = {
          dbPass: data.password,
          clientChallenge: message.challenge,
          clientPass: message.password,
          serverChallenge: hash,
          salt,
          serverHash,
        }
      
        if (message.challenge === serverHash) {
          io.emit('process', true);
        } else {
          err.type = "SQL";
          err.desc = 'Hashing Problem';
          io.emit('process', err);
        }

        
      });

    } 

  });

});

const getUsers = async function() {
  var args = {
    headers: { "Authorization": `Bearer ${glob.token}`, "Content-Type": "application/json"}
  };
  client.methods.getAllUser(args, function (data, response) {
    existing = data.map( ret => {
      return ret.username;
    });
    console.log('existing: ', existing);
  });
}

const saveToDB = async function(msg) {
  io.emit('process', msg);
  client.methods.postMethod(msg, function (data, response) {
    if (data && data[0] && data[0].code === 500) {
      err.type = "SQL";
      err.desc = data[0].details;
      io.emit('process', err);
    } else {
      io.emit('process', data);
    }
    
    
  });

}

function getHash(string){
  var hmac = crypto.createHmac('sha256', `${glob.secret}`);
  hmac.update(string); 
  return hmac.digest('hex'); 
};

server.listen(4000, () => {
  console.log("socket io is listening to port 4000");
});