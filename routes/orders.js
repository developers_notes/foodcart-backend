var express = require('express');
var router = express.Router();
var app = express();

/* GET users listing. */
router.get('/', function(req, res, next) {
  

  const http = require('http');
  const server = http.Server(app);

  const socketIO = require('socket.io');
  const io = socketIO(server);

  const port = process.env.PORT || 5000;


  io.on('connection', (socket) => {
      console.log('user connected');
  });

  io.on('message', (message) => {
    io.emit(message);
  });

  server.listen(port, () => {
      console.log(`started on port: ${port}`);
  });

});

module.exports = router;
