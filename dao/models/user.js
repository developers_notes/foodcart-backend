const mongoose = require('mongoose');
const connection = require('../connection');
const { userDateSchema } = require('./common/userDate');
const { ObjectId } = mongoose;

const farmSchema = new mongoose.Schema({ 
    username: { type: String, required: true },
    firstname: { type: String, required: false },
    lastname: { type: String, required: false },
    password: { type: String, required: false },
    published: { type: userDateSchema, required: false },
    created: { type: userDateSchema, required: true },
    updated: { type: userDateSchema, required: false },
}, {
    collection: 'users',
});

const farm = connection.db.model('exam', farmSchema);

module.exports = { farm };