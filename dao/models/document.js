const mongoose = require('mongoose');
const connection = require('../connection');
const { userDateSchema } = require('./common/userDate');

const documentSchema = new mongoose.Schema({ 
    name: { type: String, required: true },
    size: { type: Number, required: false },
    typeCode: { type: String, required: false },
    description: { type: String, required: false },
    status: { type: String, required: true },
    created: { type: userDateSchema, required: true },
    archived: { type: userDateSchema, required: false },
    deleted: { type: userDateSchema, required: false },
    visibility: { type: String, required: true, default: 'public' },
});

const document = connection.db.model('document', documentSchema);

module.exports = { document };