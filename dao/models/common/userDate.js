const mongoose = require('mongoose');

module.exports.userDateSchema = new mongoose.Schema({
    user: { type: String, required: false },
    when: { type: Date, required: false }
});