const mongoose = require('mongoose');
const config = require('config');

initDbConnect = () => {
    const db = config.get('mongodb');
    const { uri, options } = db;
 
    const c = mongoose.createConnection(uri, options);

    c.on('connected', () => {
        console.log('connected');
    });

    c.on('error', ()=> {
        console.log('error');
    })

    c.on('disconnected', () => {
        console.log('disconnect')
    });

    process.on('SIGINT', () => {
        sigintTrapped = true;
        c.close(() => {
          process.exit(0);
        });
    });

    return c;
}

module.exports = { db: initDbConnect() };